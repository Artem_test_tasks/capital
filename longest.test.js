const longest = require('./longest')

test('Empty input string', () => {
  expect(longest('')).toBe('')
});

test('One simbol input string', () => {
  expect(longest('a')).toBe('a')
});

test('Some long input string', () => {
  expect(longest('Hi!!! its me')).toBe('i!!!')
});

test('Some long input string, but 4 diff chars allowed', () => {
  expect(longest('Hey! Hi!!! its me', 4)).toBe('! Hi!!! i')
});

test('O(n)', () => {
  const str = '12345678911000121'
  const spy = jest.spyOn(Object, 'keys')
  longest(str)
  expect(spy.mock.calls.length).toBeLessThanOrEqual(2 * str.length)
});



