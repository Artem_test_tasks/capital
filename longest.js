/*
 * Ф-ция возвращает подстроку макс длины, состоящую не более чем из n уникальных символов
 *
 * x - левый, у  - правый указатель.
 *
 * Двигаем правый указатель 
 * если текущий символ не нарушает правила (не более n уникальных символов), то 
 * повторям (двигаем правый указатель)
 * иначе двигаем левый указатель 
 *
 */
const longest = (str, n = 2) => {
  let x = 0
  let y = 0
  let [bestX, bestY] = [x, y]
  const charMap = {}

  let moveY = true
  while (y < str.length) {
    // Если двигаем указатель Y, то запоминаем символ в map (инкременитируем счетчик)
    if (moveY) charMap[str[y]] = (charMap[str[y]] || 0) + 1

    moveY = Object.keys(charMap).length <= n

    if (moveY) {
      // Если текущaя цепочка длиннее, запомним х и y
      if (y - x > bestY - bestX) [bestX, bestY] = [x, y]
      y++
    } else {
      // Удалим из карты символ (уменьшим счетчик. и если он =0 удалим ключ вообще)
      let c = str[x]
      charMap[c]--
      if (charMap[c] === 0) delete charMap[c]
      if (x < y) x++
    }
  }
  return str.substring(bestX, bestY + 1)
}

module.exports = longest
